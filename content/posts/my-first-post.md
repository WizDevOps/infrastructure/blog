---
title: "Init repo"
date: 2019-10-27T11:23:40+01:00
toc: false
images:
tags:
  - untagged
---

It's very interesting sometimes to test Golang blogging platforms

This is a sample YAML manifest

{{< highlight yaml >}}
{{- if (and (ne (.Values.ui.ingress.enabled | toString ) "-") .Values.ui.ingress.enabled) }}
{{- $serviceName := printf "%s-%s" (include "vault.fullname" .) "ui" -}}
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: {{ template "vault.fullname" . }}-ingress
  namespace: {{ .Release.Namespace }}
  labels:
    app: {{ template "vault.name" . }}
    chart: {{ template "vault.chart" . }}
    heritage: {{ .Release.Service }}
    release: {{ .Release.Name }}
  {{- if .Values.ui.ingress.annotations }}
  annotations:
{{ toYaml .Values.ui.ingress.annotations | indent 4 }}
  {{- end }}
spec:
  rules:
    {{- range .Values.ui.ingress.hosts }}
    - host: {{ . }}
      http:
        paths:
          - backend:
              serviceName: {{ $serviceName }}
              servicePort: http
    {{- end -}}
  {{- if .Values.ui.ingress.tls }}
  tls:
    {{- range $value := .Values.ui.ingress.tls }}
    - hosts:
      {{- range $value.hosts }}
      - {{ . }}
      {{- end }}
      secretName: {{ $value.secretName }}
    {{- end }}
  {{- end }}
{{- end }}
{{< /highlight >}}

## Some python sample code

{{< highlight python >}}
#!/usr/bin/python3

from engine import RunForrestRun

"""Test code for syntax highlighting!"""

class Foo:
	def __init__(self, var):
		self.var = var
		self.run()

	def run(self):
		RunForrestRun()  # run along!
{{< /highlight >}}

## Telegram posts :smile:
> This is a sample post from one of our Telegram channels

<script async src="https://telegram.org/js/telegram-widget.js?7" data-telegram-post="golang101/1148" data-width="100%"></script>

