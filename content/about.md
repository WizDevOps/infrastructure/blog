---
title: "About"
date: 2019-10-27T11:37:20+01:00
---
Site Reliability Engineer with over 5 years of experience in IT Operations.

During my work I've done everything from Ruby on Rails programming, Python & Bash scripting, Goland coding et. al.
