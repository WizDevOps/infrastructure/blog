FROM golang:alpine as base
ENV GO111MODULE="on" \
    CGO_ENABLED="0" \
    GOOS="linux"
COPY . /app
RUN set -x \
    && apk add --update --no-cache git \
    && go install github.com/gohugoio/hugo \
    && hugo -v --source=/app --destination=/app/public

FROM nginx:stable-alpine
RUN mv /usr/share/nginx/html/index.html /usr/share/nginx/html/old-index.html
COPY --from=base /app/public /usr/share/nginx/html/
EXPOSE 80
